import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MerchandiseController } from './controllers/merchandise.controller';
import { Merchandise } from './entities/merchandise.entity';
import { MerchandiseService } from './services/merchandise.service';

@Module({
    imports:[TypeOrmModule.forFeature([Merchandise])],
    controllers :[MerchandiseController],
    providers :[MerchandiseService]
})
export class MerchandiseModule {}
