import { MerchandiseDTO } from "src/merchandise/dto/merchandise.model";

export class SizeDTO{
    id?: MerchandiseDTO;
    size:string ;
    createdAt?:string;
    updatedAt?:string;
}