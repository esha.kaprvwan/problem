import { Body, Controller, Get, Post } from "@nestjs/common";
import { SizeDTO } from "../dto/size.model";
import { SizeService } from "../services/size.service";

@Controller('size')
export class SizeController{

    constructor(private SizeService : SizeService){}
    
    @Get()
    async getAllSize(){
        const sizes= await this.SizeService.findAll().then(result=>{
            return result;
        }).catch((err)=>{
            return ("Some error occured: " +err);
        })
        console.log(sizes);
        return sizes;
    }
    
    @Post()
    async addSize(@Body() size: SizeDTO){
        console.log(size);
        const savedSize =await this.SizeService.createSize(size);
        return { data : savedSize }
    }

}