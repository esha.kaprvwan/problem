import { BaseEntity } from "src/base-entity";
import { Entity, Column, ManyToOne, JoinColumn, PrimaryGeneratedColumn } from "typeorm";
import { Merchandise } from "src/merchandise/entities/merchandise.entity";

@Entity('Size')
export class Size {

        @PrimaryGeneratedColumn('uuid')
        @ManyToOne(()=> Merchandise ,( merch : Merchandise) => merch.sizes )
        @JoinColumn()
        id:Merchandise

        @Column({ type: 'varchar', length: 3, nullable: false })
        size: String;


};

