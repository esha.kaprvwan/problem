export enum Roles {
  Admin = 'Admin',
  Appreciator = 'Appreciator',
  User = 'User',
}

export class UsersDTO {
  id: string;
  name: string;
  email: string;
  role: string[];
  points: number;
}
