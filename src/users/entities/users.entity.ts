import { BaseEntity } from 'src/base-entity';
import { Column, Entity } from 'typeorm';
import { Roles } from '../dto/users.model';

@Entity('users')
export class Users extends BaseEntity {
  @Column({ type: 'varchar', nullable: false })
  name: string;

  @Column({ type: 'varchar', nullable: false })
  email: string;

  @Column({ type: 'enum', enum: Roles, nullable: false, default: Roles.User })
  role: string[];

  @Column({ type: 'int', nullable: false })
  points: number;
}
