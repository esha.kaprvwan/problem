import { Controller, Post, Body, Get } from '@nestjs/common';
import { UsersDTO } from '../dto/users.model';
import { UsersService } from '../services/users.services';

@Controller('users')
export class UsersController {
  constructor(private userService: UsersService) {}

  @Post()
  async appendUser(@Body() user: UsersDTO) {
    const savedUser = await this.userService.createUser(user);
    return savedUser;
  }

  @Get()
  async getUser() {
    const allUsers = await this.userService.getUsers();
    return allUsers;
  }
}
