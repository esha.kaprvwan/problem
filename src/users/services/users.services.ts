import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UsersDTO } from '../dto/users.model';
import { Users } from '../entities/users.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(Users)
    private usersRepository: Repository<Users>,
  ) {}

  createUser(userDTO: UsersDTO) {
    return this.usersRepository.save(userDTO);
  }

  getUsers() {
    return this.usersRepository.find();
  }
}
