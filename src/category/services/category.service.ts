import { Category } from "../entities/category.entity";
import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { CategoryDTO } from "../dto/category.model";

@Injectable()
export class CategoryService{

    constructor(
        @InjectRepository(Category)
        private CategoryRepository: Repository<Category>
    ) { }

    async findAll():Promise<Category[]>{
        const categories= await this.CategoryRepository.find().then(result=>{
            return result;
        })
        return categories;
    }

    createCategory(categoryDTO: CategoryDTO) {
        return this.CategoryRepository.save(categoryDTO);
    }
    

}