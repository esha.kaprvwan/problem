import { BaseEntity } from "src/base-entity";
import { Entity, Column } from "typeorm";
import { Merchandise } from "src/merchandise/entities/merchandise.entity";


@Entity('category')
export class Category extends BaseEntity {

        @Column()
        category: String;


}

