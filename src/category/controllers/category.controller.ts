import { Body, Controller, Get, Post } from "@nestjs/common";
import { CategoryDTO } from "../dto/category.model";
import { CategoryService } from "../services/category.service"

@Controller('category')
export class CategoryController{

    constructor(private categoryService : CategoryService){}
    
    @Get()
    async getAllCategory(){
        const categories= await this.categoryService.findAll().then(result=>{
            return result;
        }).catch((err)=>{
            return ("Some error occured: " +err);
        })
        console.log(categories);
        return categories;
    }
    
    @Post()
    async addCategory(@Body() category: CategoryDTO){
        console.log(category);
        const savedCategory =await this.categoryService.createCategory(category);
        return { data : savedCategory }
    }

}